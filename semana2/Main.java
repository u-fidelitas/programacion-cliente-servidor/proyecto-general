package semana2;

class Main {
    public static void main(String[] args) {

        // Ejemplo:
        Empresa newEmpresa = new Empresa("Intel");

        Cliente clienteUno = new Cliente("HP", "+1 324 1243");
        Cliente clienteDos = new Cliente("Dell", "+34 324 1243");
        Cliente clienteTres = new Cliente("Lenovo", "+506 324 1243");

        newEmpresa.addCliente(clienteUno);
        newEmpresa.addCliente(clienteDos);
        newEmpresa.addCliente(clienteTres);

        System.out.println(
                "Los clientes de la empresa " + newEmpresa.getNombre() + " son:\n" + newEmpresa.getListaClientes());
    }
}