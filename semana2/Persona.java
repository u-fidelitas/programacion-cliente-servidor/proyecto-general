package semana2;

import java.util.Calendar;
import java.util.Date;

/**
 * Persona
 */
public class Persona {

    private Date fechaNacimiento;

    Persona(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public int calculaEdad() {
        int anhoActual = Calendar.getInstance().get(Calendar.YEAR);
        int anhoNacimiento = getCalendarFechaNacimiento().get(Calendar.YEAR);
        return anhoActual - anhoNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    private Calendar getCalendarFechaNacimiento() {
        Calendar fechaNacimiento = Calendar.getInstance();
        fechaNacimiento.setTime(this.fechaNacimiento);
        return fechaNacimiento;
    }
}