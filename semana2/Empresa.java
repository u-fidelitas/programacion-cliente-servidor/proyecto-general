package semana2;

import java.util.LinkedList;

/**
 * Empresa
 */
public class Empresa {

    private String nombre;
    private LinkedList<Cliente> clientes;

    public Empresa(String nombre) {
        this.nombre = nombre;
        this.clientes = new LinkedList<Cliente>();
    }

    public void addCliente(Cliente newCliente) {
        this.clientes.add(newCliente);
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return this.nombre;
    }

    public String getListaClientes() {
        String clientesString = "\n";
        for (Cliente cliente : this.clientes) {
            clientesString = clientesString.concat("\t- " + cliente.getNombre() + "\n");
        }

        return clientesString;
    }
}