package semana4;

import java.util.LinkedList;
import java.util.AbstractMap.SimpleEntry;

import javax.swing.JOptionPane;

/**
 * Empleado
 */
public class Empleado {

    public static LinkedList<Empleado> empleados = new LinkedList<Empleado>();
    protected static int cedulaError = 0;
    private static int limitEmpleados = 10;

    int cedula;
    String nombre, apellido1, apellido2, departamento;
    double horasSemanales, salarioPorHora;

    Empleado() {
        this.cedula = cedulaError;
    }

    Empleado(int cedula, String nombre, String apellido1, String apellido2, String departamento, double horasSemanales,
            double salarioPorHora) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.departamento = departamento;
        this.horasSemanales = horasSemanales;
        this.salarioPorHora = salarioPorHora;
        empleados.add(this);
    }

    public String whoIs() {
        return "E";
    }

    private double getSalarioSemanal() {
        return this.salarioPorHora * this.horasSemanales;
    }

    private double getSalarioQuincenal() {
        return this.getSalarioSemanal() * 2;
    }

    private double getSalarioMensual() {
        return (this.salarioPorHora * this.horasSemanales) * 4.33;
    }

    public String salarioToString() {
        return "Salario:\n\tHoras por semana: " + this.horasSemanales + "\n\tSalario por hora:\t¢ "
                + this.salarioPorHora + "\n\tSalario por semana:\t¢ " + this.getSalarioSemanal()
                + "\n\tSalario por quicena: " + this.getSalarioQuincenal() + "\n\tSalario por mes: "
                + this.getSalarioMensual();
    }

    public String toString() {
        return "Céd: " + this.cedula + "\nNombre: " + this.nombre + " " + this.apellido1 + " " + this.apellido2
                + ".\nDepartamento: " + this.departamento + ".";
    }

    public static Empleado getEmpleadoByCedula(int cedula) {
        for (Empleado empleado : empleados) {
            if (empleado.cedula == cedula) {
                return empleado;
            }
        }

        return new Empleado();
    }

    public static void deleteEmpleado() {
        int cedula = getUserInt("Digite la cédula del empleado que desea eliminar");
        Empleado delEmpleado = getEmpleadoByCedula(cedula);
        if (delEmpleado.cedula == cedula) {
            deleteEmpleadoByCedula(cedula);
            showMessage("El usuario " + delEmpleado.cedula + " - " + delEmpleado.nombre + " " + delEmpleado.apellido1
                    + " " + delEmpleado.apellido2 + " fue eliminado.");
            return;
        } else {
            showMessage("El empleado digitado no existe");
        }
    }

    private static Boolean deleteEmpleadoByCedula(int cedula) {
        Empleado emp = getEmpleadoByCedula(cedula);
        if (emp.cedula == cedulaError) {
            return false;
        } else {
            empleados.remove(emp);
            return true;
        }
    }

    public static String getListaEmpleados() {
        String str = "=\t=\t= Empleado =\t=\t=\n";
        for (Empleado empleado : empleados) {
            str = str.concat(empleado.toString() + '\n' + empleado.salarioToString() + '\n');
        }

        return str;
    }

    public static String getListaEmpleadosSalarios() {
        String str = "=\t=\t= Empleado =\t=\t=\n";
        for (Empleado empleado : empleados) {
            str = str.concat("Céd: " + empleado.cedula + '\n' + empleado.salarioToString() + "\n\n");
        }

        return str;
    }

    public static String getListaEmpleadosSalariosMensuales() {
        String str = "=\t=\t= Empleado =\t=\t=\n";
        for (Empleado empleado : empleados) {
            str = str.concat("Céd: " + empleado.cedula + " - ¢ " + empleado.getSalarioMensual() + "\n");
        }

        return str;
    }

    public static void createEmpleado(int jefeCed) {
        int cedula;
        String nombre, apellido1, apellido2, departamento;
        double horasSemanales, salarioPorHora;

        SimpleEntry<Boolean, String> canWeCreateAndError = verifyRestictions();
        if (!canWeCreateAndError.getKey()) {
            showMessage(canWeCreateAndError.getValue());
            return;
        }

        cedula = getUserInt("Digite la cédula del empleado");
        if (getEmpleadoByCedula(cedula).cedula == cedula) {
            showMessage("El usuario ya existe en el sistema.");
            return;
        }

        nombre = getUserText("Digite el nombre");
        apellido1 = getUserText("Digite el primer apellido");
        apellido2 = getUserText("Digite el segundo apellido");
        departamento = getUserText("Digite el departamento");

        horasSemanales = getUserDouble("Digite las horas que trabaja el empleado por semana");
        salarioPorHora = getUserDouble("Digite el salario del empleado por hora");

        Boolean isAdmin = getUserBoolean("¿El usuario es administrador?");
        if (isAdmin) { // Administrador
            new Administrador(cedula, nombre, apellido1, apellido2, departamento, horasSemanales, salarioPorHora);
            showMessage(empleadoAgregado(cedula));
            Boolean agregarEmpleados = getUserBoolean("¿Desea agregar empleados a " + nombre + "?");
            if (agregarEmpleados) {
                createEmpleado(cedula);
                return;
            }
        } else { // Subalterno
            int jefeCedula = (jefeCed == Empleado.cedulaError ? getUserInt("Digite la cédula del jefe") : jefeCed);
            Empleado jefe = getEmpleadoByCedula(jefeCedula);
            if (jefe.cedula != cedula) {
                showMessage("No pudimos encontrar al jefe, debe ingresar al jefe del empleado antes.");
            } else {
                Subalterno newEmpleado = new Subalterno(cedula, nombre, apellido1, apellido2, departamento,
                        horasSemanales, salarioPorHora);
                newEmpleado.addJefe(jefe);
                Administrador.addSubalterno(jefeCedula, newEmpleado);
            }
        }
        showMessage(empleadoAgregado(cedula));
    }

    private static String empleadoAgregado(int cedula) {
        Empleado newEmpleado = getEmpleadoByCedula(cedula);
        return "¡Empleado agregado con éxito!\n\n" + newEmpleado.toString() + '\n' + newEmpleado.salarioToString();
    }

    private static SimpleEntry<Boolean, String> verifyRestictions() {
        SimpleEntry<Boolean, String> response = new SimpleEntry<Boolean, String>(true, "");

        // Verify limit of employees
        if (empleados.size() == limitEmpleados) {
            response = new SimpleEntry<Boolean, String>(false,
                    "Se ha llegado al límite(" + limitEmpleados + " máx.) de empleados.");
        }

        return response;
    }

    /* Input Helpers */
    private static String getUserText(String message) {
        return JOptionPane.showInputDialog(message);
    }

    private static Double getUserDouble(String message) {
        String input;
        Boolean validInput = false;
        Double inputParsed = 0.0;

        while (!validInput) {
            input = getUserText(message);
            try {
                inputParsed = Double.parseDouble(input);
                validInput = true;
            } catch (Exception e) {
                validInput = false;
            }
        }

        return inputParsed;
    }

    private static int getUserInt(String message) {
        String input;
        Boolean validInput = false;
        int inputParsed = 0;

        while (!validInput) {
            input = getUserText(message);
            try {
                inputParsed = Integer.parseInt(input);
                validInput = true;
            } catch (Exception e) {
                validInput = false;
            }
        }

        return inputParsed;
    }

    private static Boolean getUserBoolean(String message) {
        String input;
        Boolean validInput = false;
        Boolean inputParsed = false;

        while (!validInput) {
            input = getUserText(message.trim() + " (S - Sí, N - No)");
            if (input.toUpperCase().equals("S")) {
                inputParsed = true;
                validInput = true;
            } else if (input.toUpperCase().equals("N")) {
                inputParsed = false;
                validInput = true;
            }
        }

        return inputParsed;
    }

    private static void showMessage(String msg) {
        JOptionPane.showMessageDialog(null, msg);
    }

}