package semana4;

import java.util.AbstractMap.SimpleEntry;

import javax.swing.JOptionPane;

/**
 * Main
 */
public class Main {

    public static void main(String[] args) {
        // MENU
        SimpleEntry<String, String> option1 = new SimpleEntry<String, String>("1", "Agregar Empleado"); // only 10
        SimpleEntry<String, String> option2 = new SimpleEntry<String, String>("2", "Eliminar Empleado"); // by ID
        SimpleEntry<String, String> option3 = new SimpleEntry<String, String>("3", "Imprimir Empleados");
        SimpleEntry<String, String> option4 = new SimpleEntry<String, String>("4", "Calcular Salario Mensual");
        SimpleEntry<String, String> option5 = new SimpleEntry<String, String>("5", "Calcular Salarios");
        SimpleEntry<String, String> option6 = new SimpleEntry<String, String>("6", "Salir");

        String[] options = new String[6];
        options[0] = option1.getValue();
        options[1] = option2.getValue();
        options[2] = option3.getValue();
        options[3] = option4.getValue();
        options[4] = option5.getValue();
        options[5] = option6.getValue();
        Menu.createMenu(options);

        boolean salir = false;
        while (!salir) {
            String option = JOptionPane.showInputDialog(null, Menu.getMenu());
            if (Menu.verifyOption(option)) {
                if (option.equals(option6.getKey())) {
                    salir = true;
                } else {
                    switch (option) {
                    case "1":
                        Empleado.createEmpleado(Empleado.cedulaError);
                        break;
                    case "2":
                        Empleado.deleteEmpleado();
                        break;
                    case "3":
                        Empleado.getListaEmpleados();
                        break;
                    case "4":
                        Empleado.getListaEmpleadosSalariosMensuales();
                        break;
                    case "5":
                        Empleado.getListaEmpleadosSalarios();
                        break;
                    default:
                        return;
                    }
                }
            } else {
                JOptionPane.showMessageDialog(null, "Digite una opción válida.");
            }
        }
    }
}