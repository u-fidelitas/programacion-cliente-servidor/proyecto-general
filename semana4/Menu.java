package semana4;

import java.util.LinkedList;
import java.util.AbstractMap.SimpleEntry;

/**
 * Menu
 */
public class Menu {

    private static LinkedList<SimpleEntry<Integer, String>> options = new LinkedList<SimpleEntry<Integer, String>>();

    public static void createMenu(String[] optionsInp) {
        int i = 1;
        options.clear();
        for (String option : optionsInp) {
            options.add(new SimpleEntry<Integer, String>(i++, option));
        }
    }

    private static String menuToString() {
        String text = "";
        for (SimpleEntry<Integer, String> option : options) {
            text = text.concat(option.getKey() + ") " + option.getValue() + '\n');
        }

        return text;
    }

    private static String listToString(LinkedList<SimpleEntry<String, LinkedList<Empleado>>> lists) {
        String listSize = "";
        for (SimpleEntry<String, LinkedList<Empleado>> objKV : lists) {
            listSize = listSize.concat(objKV.getKey() + ": " + objKV.getValue().size() + '\n');
        }

        return listSize;
    }

    public static Boolean verifyOption(String optionInp) {
        Integer option = 0;
        try {
            option = Integer.parseInt(optionInp);
            if (option > 0 && option <= options.size()) {
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    public static String getMenu(LinkedList<SimpleEntry<String, LinkedList<Empleado>>> lists) {
        return "==== MENU ====\n" + menuToString() + "--- Lista ---\n" + listToString(lists);
    }

    public static String getMenu() {
        return "==== MENU ====\n" + menuToString();
    }
}