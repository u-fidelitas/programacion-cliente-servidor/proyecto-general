package semana4;

import java.util.LinkedList;

/**
 * Administrador
 */
public class Administrador extends Empleado {

    public static String adminTag = "A";
    LinkedList<Empleado> subalternos;

    Administrador(int cedula, String nombre, String apellido1, String apellido2, String departamento,
            double horasSemanales, double salarioPorHora) {
        super(cedula, nombre, apellido1, apellido2, departamento, horasSemanales, salarioPorHora);
        this.subalternos = new LinkedList<Empleado>();
    }

    public static void addSubalterno(int cedulaJefe, Subalterno subalterno) {
        for (Empleado empleado : Empleado.empleados) {
            if (empleado.cedula == cedulaJefe && empleado.whoIs().equals(adminTag)) {
                ((Administrador) empleado).subalternos.add(subalterno);
            }
        }
    }

    public static Empleado getEmpleadoByCedula(int cedula) {
        for (Empleado empleado : empleados) {
            if (empleado.cedula == cedula) {
                return empleado;
            }
        }

        return new Empleado();
    }

    @Override
    public String whoIs() {
        return adminTag;
    }

    @Override
    public String toString() {
        return "--- Administrador ---\n" + this.toStringBasic() + "\nCantidad de subalternos: "
                + this.subalternos.size();
    }

    public String toStringBasic() {
        return "Céd: " + this.cedula + "\nNombre: " + this.nombre + " " + this.apellido1 + " " + this.apellido2
                + ".\nDepartamento: " + this.departamento + ".";
    }
}