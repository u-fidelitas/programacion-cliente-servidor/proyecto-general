package semana4;

/**
 * Subalterno
 */
public class Subalterno extends Empleado {

    Empleado jefe;

    Subalterno(int cedula, String nombre, String apellido1, String apellido2, String departamento,
            double horasSemanales, double salarioPorHora) {
        super(cedula, nombre, apellido1, apellido2, departamento, horasSemanales, salarioPorHora);
    }

    // public static void addJefe(int cedula, Empleado jefe) {
    // for (Empleado empleado : empleados) {
    // if (empleado.cedula == cedula) {
    // empleado.jefe = jefe;
    // }
    // }
    // }

    public void addJefe(Empleado jefe) {
        this.jefe = jefe;
    }

    @Override
    public String whoIs() {
        return "A";
    }

    @Override
    public String toString() {
        return "--- Empleado ---\n" + this.toString() + this.salarioToString();
    }
}