package semana3;

import java.util.LinkedList;
import java.util.AbstractMap.SimpleEntry;

import javax.swing.JOptionPane;

/**
 * Main
 */
public class Main {
    private static LinkedList<Animal> aves = new LinkedList<Animal>();
    private static LinkedList<Animal> mamiferos = new LinkedList<Animal>();
    public static LinkedList<SimpleEntry<String, LinkedList<Animal>>> lists = new LinkedList<SimpleEntry<String, LinkedList<Animal>>>();

    private static Boolean salir = false;

    public static void main(String[] args) {
        // Ejemplo: Herencia
        // Mamifero animalMamifero = new Mamifero("Perro", "H", "Pug", 6.0, 10.0, "Kg",
        // 63, "dias");
        // Ave animalAve = new Ave("Aguila", "M", "Aguila Calva", 3.0, 6.3, "Kg",
        // "blancas/negras", true);

        // System.out.println(animalMamifero.toString());
        // System.out.println(animalAve.toString());

        String aveKey = "Aves";
        String mamKey = "Mamíferos";
        lists.add(new SimpleEntry<String, LinkedList<Animal>>(aveKey, aves));
        lists.add(new SimpleEntry<String, LinkedList<Animal>>(mamKey, mamiferos));

        // MENU
        String[] options = new String[5];
        options[0] = "Crear Ave";
        options[1] = "Imprimir Aves";
        options[2] = "Crear Mamífero";
        options[3] = "Imprimir Mamíferos";
        options[4] = "Salir";
        Menu.createMenu(options);

        while (!salir) {
            String option = JOptionPane.showInputDialog(null, Menu.getMenu(lists));
            if (Menu.verifyOption(option)) {
                if (option.equals("5")) {
                    salir = true;
                } else {
                    switch (option) {
                    case "1", "3":
                        if (verifyLists(option)) {
                            JOptionPane.showMessageDialog(null,
                                    "Se ha llegado a la cantidad máxima que se puede guardar. Elija otra opción.");
                        } else {
                            createObj(option);
                        }
                        break;
                    case "2":
                        printList(aveKey);
                        break;
                    case "4":
                        printList(mamKey);
                        break;
                    default:
                        break;
                    }
                }
            } else {
                JOptionPane.showMessageDialog(null, "Digite una opción válida.");
            }
        }
    }

    private static boolean isMax(LinkedList<Animal> list, int max) {
        return list.size() < max ? false : true;
    }

    private static boolean verifyLists(String option) {
        if (option.equals("1")) {
            return isMax(getList("Aves"), 3);
        } else {
            return isMax(getList("Mamíferos"), 3);
        }
    }

    private static LinkedList<Animal> getList(String key) {
        for (SimpleEntry<String, LinkedList<Animal>> entry : lists) {
            if (entry.getKey().equals(key)) {
                return entry.getValue();
            }
        }
        return new LinkedList<Animal>();
    }

    /* Creators */
    private static void createObj(String option) {
        String nombre, sexo, raza, pesoMedida;
        Double pesoMin, pesoMax;

        nombre = getUserText("Digite el nombre");
        sexo = getUserText("Digite el sexo (H - Hembra, M - Macho)");
        raza = getUserText("Digite la raza");

        pesoMin = getUserDouble("Digite el peso mínimo");
        pesoMax = getUserDouble("Digite el peso máximo");
        pesoMedida = getUserText("Digite la medida usada para el peso (KG, G, etc.) ");

        if (option.equals("1")) {
            String colorPlumas = getUserText("Digite el color de las plumas");
            Boolean vuela = getUserBoolean("El ave vuela? (S - Sí, N - No");
            createSaveAve(nombre, sexo, raza, pesoMin, pesoMax, pesoMedida, colorPlumas, vuela);
        } else if (option.equals("3")) {
            int cantidadGestacion = getUserInt("Digite la cantidad de gestación");
            String medidaGestacion = getUserText(
                    "Digite la medidad que utilizó al ingresar la gestación (Dias, Meses, etc.)");
            createSaveMamifero(nombre, sexo, raza, pesoMin, pesoMax, pesoMedida, cantidadGestacion, medidaGestacion);
        }
    }

    private static void createSaveAve(String nombre, String sexo, String raza, Double pesoMin, Double pesoMax,
            String pesoMedida, String colorPlumas, Boolean vuela) {
        Ave newAve = new Ave(nombre, sexo, raza, pesoMin, pesoMax, pesoMedida, colorPlumas, vuela);
        aves.add(newAve);
    }

    private static void createSaveMamifero(String nombre, String sexo, String raza, Double pesoMin, Double pesoMax,
            String pesoMedida, int cantidadGestacion, String medidaGestacion) {
        Mamifero newMamifero = new Mamifero(nombre, sexo, raza, pesoMin, pesoMax, pesoMedida, cantidadGestacion,
                medidaGestacion);
        mamiferos.add(newMamifero);
    }

    /* Input Helpers */
    private static String getUserText(String message) {
        return JOptionPane.showInputDialog(message);
    }

    private static Double getUserDouble(String message) {
        String input;
        Boolean validInput = false;
        Double inputParsed = 0.0;

        while (!validInput) {
            input = getUserText(message);
            try {
                inputParsed = Double.parseDouble(input);
                validInput = true;
            } catch (Exception e) {
                validInput = false;
            }
        }

        return inputParsed;
    }

    private static int getUserInt(String message) {
        String input;
        Boolean validInput = false;
        int inputParsed = 0;

        while (!validInput) {
            input = getUserText(message);
            try {
                inputParsed = Integer.parseInt(input);
                validInput = true;
            } catch (Exception e) {
                validInput = false;
            }
        }

        return inputParsed;
    }

    private static Boolean getUserBoolean(String message) {
        String input;
        Boolean validInput = false;
        Boolean inputParsed = false;

        while (!validInput) {
            input = getUserText(message);
            if (input.toUpperCase().equals("S")) {
                inputParsed = true;
                validInput = true;
            } else if (input.toUpperCase().equals("N")) {
                inputParsed = false;
                validInput = true;
            }
        }

        return inputParsed;
    }

    private static void printList(String key) {
        for (SimpleEntry<String, LinkedList<Animal>> entry : lists) {
            if (entry.getKey().equals(key)) {
                String str = "";
                for (Animal animal : entry.getValue()) {
                    str = str.concat("\n" + animal.toString());
                }
                JOptionPane.showMessageDialog(null, str);
            }
        }
    }
}