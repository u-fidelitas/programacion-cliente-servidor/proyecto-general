package semana3;

/**
 * Animal
 */
public class Animal {

    public String nombre;
    public String sexo;
    public String raza;
    public double pesoMin;
    public double pesoMax;
    public String pesoMedida;

    Animal(String nombre, String sexo, String raza, Double pesoMin, Double pesoMax, String pesoMedida) {
        this.nombre = nombre;
        this.sexo = sexo;
        this.raza = raza;
        this.pesoMin = pesoMin;
        this.pesoMax = pesoMax;
        this.pesoMedida = pesoMedida;
    }

    @Override
    public String toString() {
        return "Nombre: " + this.nombre + '\n' + "Raza :" + this.raza + '\n' + "Sexo: " + this.sexo + '\n' + "Peso ("
                + this.pesoMedida + "): " + this.pesoMin + " - " + this.pesoMax;
    }
}