package semana3;

/**
 * Mamifero
 */
public class Mamifero extends Animal {

    public int cantidadGestacion;
    public String medidaGestacion;

    Mamifero(String nombre, String sexo, String raza, Double pesoMin, Double pesoMax, String pesoMedida,
            int cantidadGestacion, String medidaGestacion) {
        super(nombre, sexo, raza, pesoMin, pesoMax, pesoMedida);
        this.cantidadGestacion = cantidadGestacion;
        this.medidaGestacion = medidaGestacion;
    }

    @Override
    public String toString() {
        return "--- MAMIFERO ---" + '\n' + super.toString() + '\n' + "Cantidad Gestación: " + this.cantidadGestacion
                + " " + this.medidaGestacion;
    }
}