package semana3;

/**
 * Ave
 */
public class Ave extends Animal {

    public String colorPlumas;
    public Boolean vuela;

    Ave(String nombre, String sexo, String raza, Double pesoMin, Double pesoMax, String pesoMedida, String colorPlumas,
            Boolean vuela) {
        super(nombre, sexo, raza, pesoMin, pesoMax, pesoMedida);
        this.colorPlumas = colorPlumas;
        this.vuela = vuela;
    }

    @Override
    public String toString() {
        return "--- AVE ---" + '\n' + super.toString() + '\n' + "Color Plumas: " + this.colorPlumas + '\n' + "Vuela: "
                + (this.vuela ? "Sí" : "No");
    }
}